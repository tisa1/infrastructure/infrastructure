module "cluster_1" {
  source = "./digital-ocean"

  region       = var.digital_ocean.region
  do_token     = var.digital_ocean.do_token
  cluster_name = var.digital_ocean.cluster_name
}

module "navigation-backend" {
  source = "./gitlab"

  token        = var.gitlab.token
  namespace_id = var.gitlab.namespace_id
  name         = "navigation"
  variables = [
    {
      key   = "MONGO_URI"
      value = module.mongodb.connection_strings[0].standard_srv
    },
    {
      key           = "KUBECONFIG"
      value         = module.cluster_1.kubeconfig.kube_config[0].raw_config
      variable_type = "file"
    }
  ]
}

module "mongodb" {
  source = "./mongodb"

  public_key         = var.mongodb.public_key
  private_key        = var.mongodb.private_key
  org_id             = var.mongodb.org_id
  prod_user_password = var.mongodb.prod_user_password
  dev_user_password  = var.mongodb.dev_user_password
  name               = "cta"
}


output "mongo_uri" {
  value = module.mongodb.connection_strings[0].standard_srv
}

output "kubeconfig" {
  value     = module.cluster_1.kubeconfig.kube_config[0].raw_config
  sensitive = true
}
