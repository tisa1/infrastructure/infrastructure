resource "mongodbatlas_custom_db_role" "production" {
  project_id = mongodbatlas_project.project.id
  role_name  = "CTA"

  actions {
    action = "UPDATE"
    resources {
      collection_name = "users"
      database_name   = "cta"
    }
  }
  actions {
    action = "INSERT"
    resources {
      collection_name = "users"
      database_name   = "cta"
    }
  }
  actions {
    action = "REMOVE"
    resources {
      collection_name = "users"
      database_name   = "cta"
    }
  }
  actions {
    action = "FIND"
    resources {
      collection_name = "users"
      database_name   = "cta"
    }
  }
}

resource "mongodbatlas_custom_db_role" "development" {
  project_id = mongodbatlas_project.project.id
  role_name  = "CTA-Developers"

  actions {
    action = "UPDATE"
    resources {
      collection_name = "users"
      database_name   = "cta-dev"
    }
  }
  actions {
    action = "INSERT"
    resources {
      collection_name = "users"
      database_name   = "cta-dev"
    }
  }
  actions {
    action = "REMOVE"
    resources {
      collection_name = "users"
      database_name   = "cta-dev"
    }
  }
  actions {
    action = "FIND"
    resources {
      collection_name = "users"
      database_name   = "cta-dev"
    }
  }
}