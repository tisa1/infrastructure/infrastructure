output "mongo_uri" {
  description = "Mongo URI"
  value       = mongodbatlas_cluster.cluster.mongo_uri
}
output "connection_strings" {
  description = "Connection strings"
  value       = mongodbatlas_cluster.cluster.connection_strings
}