variable "public_key" {
    type=string
}

variable "private_key" {
    type=string
}

variable "name" {
    type=string
}

variable "org_id" {
    type=string
}

variable "prod_user_password" {
    type=string
}

variable "dev_user_password" {
    type=string
}
