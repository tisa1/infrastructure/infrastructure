resource "mongodbatlas_database_user" "production" {
  username           = "cta"
  password           = var.prod_user_password
  project_id         = mongodbatlas_project.project.id
  auth_database_name = "admin"

  roles {
    role_name     = "CTA"
    database_name = "admin"
  }


  scopes {
    name   = mongodbatlas_cluster.cluster.name
    type = "CLUSTER"
  }
}

resource "mongodbatlas_database_user" "development" {
  username           = "cta-dev"
  password           = var.dev_user_password
  project_id         = mongodbatlas_project.project.id
  auth_database_name = "admin"

  roles {
    role_name     = "CTA-Developers"
    database_name = "admin"
  }


  scopes {
    name   = mongodbatlas_cluster.cluster.name
    type = "CLUSTER"
  }
}