# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/mongodb/mongodbatlas" {
  version     = "0.9.0"
  constraints = "0.9.0"
  hashes = [
    "h1:q9UMZ+3whp2AMs1ptBSpGWWH6OZnQ0xoeor1IRL1nbw=",
    "zh:383f53a547a0499b6fdb65ee3bb6c62730e8a7fad4e2a1028fe44cc5c51e5443",
    "zh:3c5986ecc289aa67bd64e9fcb6895a240df076a8d784880de1674a768f871bf4",
    "zh:41aa70ffd2fa812a4cd6604dc7321323854ebe919fa96533478d0eff95503642",
    "zh:447bd4409344541b6c9f79d9da62e8f45356fdd8c7b596de49e4547bd94c69db",
    "zh:4b98f53ec13249fcabc70e708fdacbace188792c8e565a41d885d1ef98e453ad",
    "zh:4c04fe8b0f603352076db0c7d577cbe16d4dc8a577051819637da0ca8fc77859",
    "zh:4d83054bdeaa0557a83da08ba8ddc090a57dbc0cc236818b30eb1cfb7175b743",
    "zh:4f5a9bbc8959579037c3ae04efcfb509eaa7627744318bf297fdf812d11fde31",
    "zh:656585567abf813681ce47dd0d50a4ca0f7d636795681bdc59011d5fa3513f6f",
    "zh:bdad530521f53efd701bbbefe28d4c7e38ad0b620a7568beeb4e4690d1c451ca",
    "zh:d3c2ae2f7d2dd8b1a0ea0dc97c7d9109a0fe2e3e048e5f8a62adfa3dabf1b254",
    "zh:dc00adf5f80da98d12be3ebbfb413bc9e52b60cf8f46056abab8d754d09b74b5",
    "zh:ff34474be7c6bc5efd462d03cea20063e4bbeba28acd106738c1ec7951835c8d",
  ]
}
