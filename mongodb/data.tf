data "mongodbatlas_cluster" "cluster" {
  project_id = mongodbatlas_cluster.cluster.project_id
  name       = mongodbatlas_cluster.cluster.name
}
