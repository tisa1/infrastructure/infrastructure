# Configure the GitLab Provider
provider "gitlab" {
    token = var.token
}

terraform {
  experiments = [module_variable_optional_attrs]

  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.6.0"
    }
  }
}