variable "token" {
  type = string
}

variable "namespace_id" {
  type = string
}

variable "name" {
  type = string
}

variable "variables" {
  type = list(object({
    key           = string,
    value         = string,
    variable_type = optional(string)
  }))
}
