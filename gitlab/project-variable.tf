resource "gitlab_project_variable" "project_variable" {
  count         = length(var.variables)
  project       = gitlab_project.project.id
  key           = var.variables[count.index].key
  value         = var.variables[count.index].value
  protected     = true
  variable_type = var.variables[count.index].variable_type != "env_var" ? var.variables[count.index].variable_type : "env_var"
}
