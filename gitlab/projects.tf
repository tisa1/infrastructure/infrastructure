resource "gitlab_project" "project" {
  name         = var.name
  namespace_id = var.namespace_id

  visibility_level = "private"
}
