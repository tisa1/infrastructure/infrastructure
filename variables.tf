// REUSABLE
variable "project_name" {
  type    = string
  default = ""
}

// DIGITAL_OCEAN
variable "digital_ocean" {
  type = object({
    do_token  = string,
    region = string,
    cluster_name      = string
  })
}

// MONGODB
variable "mongodb" {
  type = object({
    public_key         = string,
    private_key        = string,
    org_id             = string,
    prod_user_password = string,
    dev_user_password  = string,
  })
}


// GITLAB
variable "gitlab" {
  type = object({
    token        = string,
    namespace_id = string,
  })
}
