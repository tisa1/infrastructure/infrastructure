output "kubeconfig" {
  description = "Kubernetes kubeconfig"
  value       = digitalocean_kubernetes_cluster.cluster
}
